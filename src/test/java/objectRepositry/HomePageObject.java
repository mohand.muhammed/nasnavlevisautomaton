package objectRepositry;

import javax.xml.xpath.XPath;

import org.apache.commons.compress.archivers.zip.X000A_NTFS;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageObject {
private static final String xpath = null;
WebDriver chrome;
public  HomePageObject(WebDriver chrome) {
	
	this.chrome = chrome;
	PageFactory.initElements(chrome, this);
}

@FindBy(xpath="//div[@class='Navigation_Navigation__nH31q']//button[2]")
WebElement menuBarMenCategory;

@FindBy(xpath="//div[@class='Navigation_Navigation__nH31q']//button[1]")
WebElement menuBarWoMenCategory;

@FindBy(xpath="//input[@placeholder='Email']")
WebElement userName;

@FindBy(xpath = "//input[@placeholder='Password']")
WebElement LpassWord;

@FindBy(xpath="//span[contains(text(),'Login')]")
public WebElement loginRegisterButton;

//BelowItem Works in 2 pages login/Register
@FindBy(xpath="//button[@class='sallab-submit-button']")
WebElement submit;

@FindBy(xpath="//a[contains(text(),'Feedback')]")
WebElement feedBackBtn;

@FindBy(xpath="//a[contains(text(),'Track order')]")
WebElement trackOrderBtn;

@FindBy(xpath="//a[contains(text(),'Find a store')]")
WebElement findStoreBtn;

@FindBy(xpath="//a[contains(text(),'Help')]")
WebElement helpBtn;

@FindBy(xpath="//input[@placeholder='Gift card']")
WebElement giftCardBtn;

@FindBy(xpath="//input[@placeholder='search']")
WebElement searchInput;

@FindBy(xpath="//button[@class='ButtonWithIcon_ButtoWnithIcon__3DcJC ButtonWithIcon_ButtoWnithIcon__1GycF']//img")
WebElement CartButton;



//RegisterItems
@FindBy(xpath="//*[@id='root']/div[2]/main/div/div/form/div/a")
WebElement CreatAccount;
@FindBy(xpath ="//input[@placeholder='Name']")
WebElement Rname;
@FindBy(xpath ="//input[@placeholder='Email']")
WebElement REmail;
@FindBy(xpath ="//input[@placeholder='Password']")
WebElement RpassWord;
@FindBy(xpath ="//input[@placeholder='Confirm Password']")
WebElement CopassWord;
//Browsing Items
@FindBy(xpath="//div[@class='BrowseByCategory_BrowseByCategory__29ost']//a[2]//div[1]")
public WebElement MenSectionBtn;
@FindBy(xpath="//div[@class='BrowseByCategory_BrowseByCategory__29ost']//a[1]//div[1]")
public WebElement WoMenSectionBtn;
@FindBy(xpath = "//button[@Class='ProductsGrid_showMore__UaYWk']")
public WebElement MoreBtn;









//Register Elements
public WebElement CreatAccount () {
	return CreatAccount;
}
public WebElement Rname () {
	
	return Rname;

}
public WebElement REmail () {
	return REmail;
}

public WebElement RpassWord () {
	return RpassWord;
}

public WebElement CopassWord () {
	return CopassWord;
}


//Categories Item 

public WebElement menuItems () {
	return menuBarMenCategory;
}

//login Item 
public WebElement loginBtn () {
	return loginRegisterButton;
}

public WebElement RegisBtn () {
	return loginRegisterButton;
}

public WebElement userName () {
	return userName;
}

public WebElement lpassWord () {
	return LpassWord;
}

public WebElement submit () {
	return submit;
}
//SearchBtn
public WebElement searchInput () {
	return searchInput;
}
//Browsing
public WebElement MenSection () {
	return MenSectionBtn;
}
public WebElement WoMenSection () {
	return WoMenSectionBtn;
}

public WebElement MoreBtn () {
	return MoreBtn;
}







}
/*
By menuBarMenCategory = By.xpath("//div[@class='Navigation_Navigation__nH31q']//button[2]");
By menuBarWoMenCategory = By.xpath("//div[@class='Navigation_Navigation__nH31q']//button[1]");
By userName = By.xpath("//input[@placeholder='Email']");
By loginRegisterButton = By.xpath("//span[contains(text(),'Login')]");
By feedBackBtn= By.xpath("//a[contains(text(),'Feedback')]");
By trackOrderBtn = By.xpath("//a[contains(text(),'Track order')]");
By findStoreBtn= By.xpath("//a[contains(text(),'Find a store')]");
By helpBtn= By.xpath("//a[contains(text(),'Help')]");
By giftCardBtn= By.xpath("//a[contains(text(),'Gift card')]");
By searchInput = By.xpath("//input[@placeholder='search']");
By CartButton= By.xpath("//button[@class='ButtonWithIcon_ButtoWnithIcon__3DcJC ButtonWithIcon_ButtoWnithIcon__1GycF']//img");
By MenSection = By.xpath("//div[@class='BrowseByCategory_BrowseByCategory__29ost']//a[2]//div[1]");
By woMenSection = By.xpath("//div[@class='BrowseByCategory_BrowseByCategory__29ost']//a[1]//div[1]");

*/

