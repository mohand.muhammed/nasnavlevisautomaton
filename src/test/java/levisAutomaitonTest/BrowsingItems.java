package levisAutomaitonTest;

import org.testng.annotations.Test;

import objectRepositry.HomePageObject;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class BrowsingItems extends Base {

java.util.List<WebElement> products;
	  public static final String RESET = "\033[0m";

	public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String PURPLE = "\033[0;35m";  // PURPLE
    JavascriptExecutor js = (JavascriptExecutor) chrome;

	@Test(enabled = true, priority = 0)
	public void browseManSection() throws InterruptedException {
	    JavascriptExecutor js = (JavascriptExecutor) chrome;
		HomePageObject b= new HomePageObject(chrome);
		System.out.println(ANSI_YELLOW +"Testing BrowsingManSection"+RESET);
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		chrome.manage().deleteAllCookies();
		chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		b.MenSection().click();
		WebDriverWait wait = new WebDriverWait(chrome, 10);
//		wait.until(ExpectedConditions
//				.visibilityOfAllElementsLocatedBy(By.tagName("img")));

		products = chrome.findElements(By.cssSelector("div[Class='ProductBox_productData__23eEb']"));
		b.MoreBtn.click();

		for (int i = 0; i < products.size(); i++) {

			String[] name = products.get(i).getText().split("-");
			System.out.println(products.get(i).getText());

			if (!b.MoreBtn.getText().contains("Show more")) {
				System.out.println("No More Item ");

				break;
			}
			else {
				b.MoreBtn.click();
		        js.executeScript("arguments[0].scrollIntoView();", b.MoreBtn);
		        int x= products.size();
x+=products.size();
				Thread.sleep(1000);
			}

		}

	}

	@Test(enabled = true, priority = 1)
	public void browseWoMenSection() throws InterruptedException {
		HomePageObject b= new HomePageObject(chrome);

		System.out.println(PURPLE+"Testing BrowseForWoManSectionn"+PURPLE);

		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		chrome.manage().deleteAllCookies();
		chrome.navigate().to("https://uat.nasnav.org/levis/");
		chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		b.WoMenSection().click();
		WebDriverWait wait = new WebDriverWait(chrome, 10);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.cssSelector("div[Class='ProductBox_productData__23eEb']")));
		b.MoreBtn.click();
		products = chrome.findElements(By.cssSelector("div[Class='ProductBox_productData__23eEb']"));
	
		for (int i = 0; i < products.size(); i++) {

			String[] names = products.get(i).getText().split("-");

			if (products.isEmpty()) {
				System.out.println("No More Item ");

				break;
				
			}

			else {
				System.out.println(products.get(i).getText());

		        js.executeScript("arguments[0].scrollIntoView();", b.MoreBtn);
				Thread.sleep(1000);

				b.MoreBtn.click();
				continue;
			}
		}
	}
}
