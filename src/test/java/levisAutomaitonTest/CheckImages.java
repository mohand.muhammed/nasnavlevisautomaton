package levisAutomaitonTest;

import java.awt.Choice;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import objectRepositry.HomePageObject;

public class CheckImages extends Base {
	private static Logger log = LogManager.getLogger(CheckLinks.class.getName());

	@Test(priority = 0)
	public void checkImagesOnHP() throws IOException {
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebDriverWait wait = new WebDriverWait(chrome, Duration.ofSeconds(10),Duration.ofSeconds(2));	
		wait.until(ExpectedConditions.attributeContains(By.tagName("img"),"src","https"));
		
		List<WebElement> allImages = chrome.findElements(By.tagName("img"));

		Thread.yield();
		System.out.println("Total links are " + allImages.size());
		List<String> links = new ArrayList<>(allImages.size());

		Thread.yield();
		for (int IElement = 0; IElement < allImages.size(); IElement++) {

			String url = allImages.get(IElement).getAttribute("href");
			Thread.yield();

			links.add(url);
			System.out.println("image Link from href attribute" + "\t" + url+ "\t"+ links.size());
			if (url == null) {
				
				url = allImages.get(IElement).getAttribute("src");
				Thread.yield();

			links.add(url);
				System.out.println("image Link from Src attribute" + "\t" + url+ "\t"+   links.size());
			}
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			Thread.yield();
			con.setRequestMethod("GET");
		//	con.connect();
			if (String.valueOf(con.getResponseCode()).equals("200")) {
				System.out.println("Image @ url " + url + "\t" + "exists at server");
			} else
				System.out.println("Image @ url " + url + "\t" + " does not exists at server");
		}

		Thread.yield();
	}

	@Test(priority = 1)
	public void checkImagesonMenCategory() throws IOException {
		HomePageObject chkmencat = new HomePageObject(chrome);
		
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		chrome.manage().deleteAllCookies();
		chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		chkmencat.MenSectionBtn.click();
		//chrome.findElement(By.xpath("//div[@class='BrowseByCategory_BrowseByCategory__29ost']//a[2]//div[1]")).click();
		Thread.yield();
		try {
			chkmencat.MoreBtn.click();
			//chrome.findElement(By.xpath("//button[@Class='ProductsGrid_showMore__UaYWk']")).click();

		
		List<WebElement> allImages1 = chrome.findElements(By.tagName("img"));
		System.out.println("Total links are " + allImages1.size());
		Thread.yield();
		List<String> links = new ArrayList<String>();

		for (int IElement = 0; IElement < allImages1.size(); IElement++) {
			String url1 = allImages1.get(IElement).getAttribute("href");
			links.add(url1);
			System.out.println("image Link from href attribute" + "\t" + url1);

			if (url1 == null) {
				url1 = allImages1.get(IElement).getAttribute("src");
				Thread.yield();
				links.add(url1);
				System.out.println("image Link from Src attribute" + "\t" + url1);
			}
			URL obj = new URL(url1);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			Thread.yield();
			con.setRequestMethod("GET");
			con.connect();
			if (String.valueOf(con.getResponseCode()).equals("200")) {
				System.out.println("Image @ url " + url1 + "\t" + "exists at server");
			} else
		
				System.out.println("Image @ url " + url1 + "\t" + " does not exists at server");
			 chrome.findElement(By.xpath("//button[@Class='ProductsGrid_showMore__UaYWk']")).click();
	}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
