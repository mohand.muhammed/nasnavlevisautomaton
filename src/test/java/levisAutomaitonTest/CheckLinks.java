package levisAutomaitonTest;

import org.testng.annotations.Test;


//For Better Performance Run AS JAVA Application
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.lookup.MarkerLookup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class CheckLinks extends Base {
	private static WebDriver chrome = null;
	private static Logger log = LogManager.getLogger(CheckLinks.class.getName());

	@Test(priority = 1)
	
	public void CheckLinksOnHP() throws InterruptedException {
log.debug("start debug");
		System.out.println("CheckLinksTest Started ");
		String hompage = "https://uat.nasnav.org/levis/";

		String url = "";
		HttpURLConnection huc = null;
		int respCode = 200;
		System.setProperty("webdriver.chrome.driver", "F:\\mohand\\Selenium\\chromedriver_win32 (2)\\chromedriver.exe");
//
		ChromeOptions op = new ChromeOptions();
		op.addArguments("--headless");

		chrome = new ChromeDriver(op);
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		chrome.manage().deleteAllCookies();
//chrome.manage().window().maximize();
		chrome.get(hompage);

		List<WebElement> links = chrome.findElements(By.tagName("a"));
		Thread.yield();

		Iterator<WebElement> it = links.iterator();
		Thread.yield();

		// chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		while (it.hasNext()) {
			url = it.next().getAttribute("href");
			System.out.println(url);
			if (url == null || url.isEmpty()) {
				System.out.println("UrL is Empty Or Not Linked " + url + respCode);
				continue;
			}
			if (url.startsWith(hompage)) {
				System.out.println("Refer to Same Domain ." + url);
				continue;

			}
			Thread.yield();
//Check Connectivity of urls 
			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());

				huc.setRequestMethod("HEAD");

				huc.connect();

				respCode = huc.getResponseCode();
				if (respCode >= 400) {
					System.out.println("Code IS :   " + respCode + "\t" + "TestedUrl= " + "\t" + url + "\t"
							+ " is a NotExist link");
				} else {
					System.out.println(
							"Code IS :   " + respCode + "\t" + "TestedUrl= " + "\t" + url + "\t" + " is a valid link");
				}
				log.atError();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		log.atInfo();
		chrome.quit();
	}
}
