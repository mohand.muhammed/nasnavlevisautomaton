package levisAutomaitonTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import objectRepositry.HomePageObject;

public class RegisterToLevis extends Base {
	
	public static final String ANSI_YELLOW = "\u001B[33m";
	
	@Parameters({ "Name", "Password", "CoPassword", "Email" })
	@Test(priority = 1)
	public void Register(String Name, String Password, String CoPassword, String Email) 
	{
		System.out.println(ANSI_YELLOW+"Register Test is Starting "+ANSI_YELLOW);
		HomePageObject reg= new HomePageObject(chrome);
		reg.RegisBtn().click();
		 WebDriverWait wait = new WebDriverWait(chrome, 5);
		 wait.until(ExpectedConditions.visibilityOf(reg.loginRegisterButton));
		 System.out.print(chrome.getCurrentUrl());
		reg.CreatAccount().click();
		System.out.print(chrome.getCurrentUrl());
		reg.userName().sendKeys(Name);
		reg.REmail().sendKeys(Email);
		reg.RpassWord().sendKeys(Password);
		reg.CopassWord().sendKeys(CoPassword);
		reg.submit().click();
		
	System.out.println("Register Done "+ "\t"+ chrome.getCurrentUrl());
	}
	
}
