package levisAutomaitonTest;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import objectRepositry.HomePageObject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Login extends Base {
	@Parameters({ "userName", "passWord" })
	@Test(priority = 1)
	public void LoginToSite(String userName, String passWord) throws InterruptedException {
		System.out.println("Testing LoginFunction");
		WebDriverWait wait = new WebDriverWait(chrome, 30);
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		HomePageObject l = new HomePageObject(chrome);
		l.loginBtn().click();
		l.userName().click();
		l.userName().sendKeys(userName);
		l.lpassWord().sendKeys(passWord);
		System.out.print(chrome.getCurrentUrl());
		l.submit().click();
		System.out.println(chrome.getCurrentUrl());

		return;

	}
}
