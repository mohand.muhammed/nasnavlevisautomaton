package levisAutomaitonTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.apache.logging.log4j.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;

public class Base {
	private static Logger log = LogManager.getLogger(CheckLinks.class.getName());

	public static WebDriver chrome;
	  public static final String RESET = "\033[0m";
	@Parameters({ "Url"})
	@BeforeClass
	public void Setup(String Url) {
        WebDriverManager.chromedriver().setup();
		chrome = new ChromeDriver();
		chrome.get(Url);
		log.info("object ");

		chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		chrome.manage().window().maximize();
		chrome.manage().deleteAllCookies();
		// op.addArguments("--headless");
		// chrome.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		// chrome.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	

	@AfterClass
	public void tearDow() {
		chrome.quit();
	}
}