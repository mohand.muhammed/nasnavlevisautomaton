package levisAutomaitonTest;

import org.testng.annotations.Test;

import objectRepositry.HomePageObject;

import org.testng.AssertJUnit;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Search extends Base {

	java.util.List<WebElement> searchedItem;
	CharSequence searchCriteria = "J";
	@Test(priority = 1)
	public void SearchFn() throws InterruptedException {
		HomePageObject s = new HomePageObject(chrome);
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println("Starting Search Function Test ");
		String expectedUrl = "https://uat.nasnav.org/levis/search/results/j";
		s.searchInput().click();
		s.searchInput().clear();
		s.searchInput().sendKeys("J");
		s.searchInput().sendKeys(Keys.ENTER);
		searchedItem = chrome.findElements(By.cssSelector("div[Class='ProductBox_productData__23eEb']"));
		WebDriverWait wait = new WebDriverWait(chrome, 4);
		wait.until(ExpectedConditions
				.presenceOfAllElementsLocatedBy(By.cssSelector("div[Class='ProductBox_productData__23eEb']")));

		Iterator<WebElement> it = searchedItem.iterator();
		while (it.hasNext()) {
			System.out.println(it.next().getText());
			if (it.toString().contains("j")) {
				System.out.println("Search Contains Search Criteria  " + it.toString());
				continue;
			}

			else {
				System.out.println("Search is Getting another Result ");
				break;
			}

		}
		System.out.println("for check result");

		for (int i = 0; i < searchedItem.size(); i++) {
			String[] names = searchedItem.get(i).getText().split("-");
			Thread.sleep(3000);

			if (searchedItem.get(i).getText().toUpperCase().contains(searchCriteria.toString())) {
				System.out.println("Search Contains Search Criteria" + "\t" + searchedItem.get(i).getText().trim());
				continue;
			} else {
				System.out.println("Search Does Not Contains Search Criteria" + "\t" + searchedItem.get(i).getText());
				break;

			}
		}
		Thread.yield();

		String currentUrl = chrome.getCurrentUrl();
		System.out.println(chrome.getTitle() + s.searchInput().getText());
		AssertJUnit.assertEquals(expectedUrl, currentUrl);

		System.out.println(expectedUrl + "\t" + currentUrl);

	}

}
