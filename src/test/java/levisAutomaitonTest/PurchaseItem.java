package levisAutomaitonTest;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bsh.Console;

import java.awt.Color;
import java.time.Duration;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class PurchaseItem extends Base {
	@Parameters({ "userName", "passWord", "mobileNumber", "country", "city", "address" })
	@Test(priority = 1)
	private void PurchaseItemByCategory(String userName, String passWord, String mobileNumber, String country,
			String city, String address) throws InterruptedException {
		// TODO Auto-generated method stub
		System.out.println("Testing PuurchaseItemFunction");

		chrome.manage().deleteAllCookies();
		chrome.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		WebDriverWait wait = new WebDriverWait(chrome, 30);
		chrome.findElement(By.xpath("//div[@class='BrowseByCategory_BrowseByCategory__29ost']//a[2]//div[1]")).click();
		Thread.sleep(5000);
		WebElement purchasedItem = chrome.findElement(By.xpath("//h6[contains(text(),'1,600 EGP')]"));
		// System.out.println("Item Purchases Is :" + "\t" + purchasedItem.getText());

		purchasedItem.click();

		System.out.println(chrome.getTitle());

		WebElement AddToCart = chrome.findElement(By.xpath("//button[@class='ProductDetails_addToCart__MNxpw']"));
		AddToCart.click();
		Thread.sleep(5000);

		System.out.println("ItemPurchased");
		Thread.sleep(5000);

		// System.out.println(chrome.findElement(By.xpath("/html/body/div[2]/div/div")).getText());
		// System.out.println(chrome.switchTo().alert().getText());

		WebElement counter = chrome.findElement(By.cssSelector("p[Class='Cart_badge__Li1q-']"));

		System.out.println(counter.getText() + "\t" + "Item Purchased");
		chrome.findElement(By.cssSelector(
				"button[Class='ButtonWithIcon_ButtoWnithIcon__3DcJC ButtonWithIcon_ButtoWnithIcon__1GycF']")).click();
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated(By.cssSelector("//div//a[@class='DropCart_cartDropdownBtn__399lD']")));
		Thread.sleep(5000);

		chrome.findElement(By.cssSelector(
				"div.App:nth-child(2) div.Menu_Menu__2vYBJ div.Wrapper_Wrapper__3zK2z.Menu_middlebar__3ENlG:nth-child(2) div.Menu_links__oaq3k div.Cart_Cart__37aXJ:nth-child(2) > p.Cart_badge__Li1q-"))
				.click();

		Thread.sleep(5000);
		System.out.println(chrome.getCurrentUrl());

		List<WebElement> itemsOnCard = chrome.findElements(By.xpath("//div[@class='BoxItem_BoxItem__2YBeQ']"));
		Iterator<WebElement> it = itemsOnCard.iterator();

		for (int i = 0; i < itemsOnCard.size(); i++) {

			String[] name = itemsOnCard.get(i).getText().split("-");
			System.out.println(itemsOnCard.get(i).getText());
		}

		chrome.findElement(By.xpath("//div[@class='Cart_Cart__37aXJ']")).click();
		Thread.sleep(2000);

		// ("//span[contains(text(),'Cart')]")).click();
		chrome.findElement(By.xpath("//a[@class='DropCart_cartDropdownBtn__399lD']")).click();
		chrome.findElement(By.xpath("//button[contains(text(),'checkout')]")).click();

		System.out.println(chrome.getCurrentUrl());
		chrome.findElement(By.xpath("//input[@placeholder='Email']")).sendKeys(userName);
		chrome.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys(passWord);
		chrome.findElement(By.xpath("//button[@class='sallab-submit-button']")).click();

		System.out.println(chrome.getCurrentUrl());
		// Assert.assertEquals(chrome.getCurrentUrl(),
		// "https://uat.nasnav.org/levis/checkout");
		chrome.findElement(By.xpath("//input[@placeholder='Contact Name']")).getText();
		WebElement CountryDropDown = chrome.findElement(By.name("country"));
		Select s = new Select(CountryDropDown);
		s.selectByValue(country);
		WebElement cityDropDown = chrome.findElement(By.xpath("//select[@name='city']"));
		Thread.sleep(5000);

		cityDropDown.click();
		Select s2 = new Select(cityDropDown);
		s2.selectByValue(city);
		chrome.findElement(By.xpath("//input[@placeholder='Address']")).sendKeys(address);
		chrome.findElement(By.xpath("//input[@placeholder='Mobile Number']")).sendKeys(mobileNumber);

		chrome.findElement(By.xpath("//button[@class='form-submit']")).click();

		Thread.sleep(5000);
		LogEntries logs = chrome.manage().logs().get("browser");
		for (LogEntry log : logs) {
			System.out.println(new Date(log.getTimestamp()) + " " + log.getLevel() + " " + log.getMessage());
			// do something useful with the data
		}

		// WebDriverWait W = new WebDriverWait(chrome,10);

		// button[text()='checkout']

	}// select[@name='country']

}
